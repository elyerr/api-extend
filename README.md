## Api-Extend
Extensión para API bajo el framework de Laravel. Esta extensión funciona usando **Sanctum** 

## Instalar version stable
```
composer require elyerr/api-extend
```

## Instalar version en desarrollo
```
composer require elyerr/api-extend dev-main
```

## Installar lo necesario
```
php artisan api-extend:install
```

## Instalar controladores de authenticacion
```
php artisan api-extend:auth
```

## [Ver Documentacion](https://gitlab.com/elyerr/api-extend/-/wikis/home)
